### Laravel Flash Message

## Install

Add the providers on config/app.php file

    Backtheweb\Flash\FlashServiceProvider::class

And the facade alias
    
    'Flash' =>  Backtheweb\Flashs\Facade::class

## Publish


    php artisan vendor:publish --provider="Backtheweb\Flash\FlashServiceProvider" --tag="config"
        
    php artisan vendor:publish --provider="Backtheweb\Flash\FlashServiceProvider" --tag="views"
